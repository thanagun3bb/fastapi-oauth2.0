import re
from langdetect import detect
def checkPassword(password: str):
    flag: int = 0
    message = "Valid password"
    while True:  
        if (len(password)<3 or len(password)>20):
            flag = -1
            message = "Password require must be between 3 - 20 characters."
            break
        elif not re.search("[a-z]", password):
            flag = -1
            message = "Require upper Character for Password."
            break
        elif not re.search("[A-Z]", password):
            message = "Require lower Character for Password."
            flag = -1
            break
        elif not re.search("[0-9]", password):
            message = "Require character not only number for Password."
            flag = -1
            break
        elif re.search("\s", password):
            flag = -1
            message = "invalid Password"
            break
        else:
            flag = 0
            message = "Valid Password"
            break

        # Require for email is username
        # if re.search("[_@$]", username):
        #     flag = -1
        #     message = "require email sylmbol."
        #     break
    if flag ==-1:
        print(message)
        return False, message
    else:
        print(message)
        return True, message

def checkUsername(username: str): 
    flag: int = 0
    message = "Valid username"
    while True:  
        if (len(username)<3 or len(username)>20):
            flag = -1
            message = "Username require must be between 3 - 20 characters."
            break
        elif re.search("[_@$]", username):
            flag = -1
            message = "Don't require need sylmbol in username."
            break
        elif re.search("\s", username):
            flag = -1
            break
        else:
            flag = 0
            break
    if flag ==-1:
        print(message)
        return False, message
    else:
        print(message)
        return True, message

def checkEmail(email: str):
    flag: int = 0
    message = "Valid email"
    # Make a regular expression
    # for validating an Email
    regex = '^(\w|\.|\_|\-)+[@](\w|\_|\-|\.)+[.]\w{2,3}$'
    if re.search(regex, email):
        flag = 0
        message = "Valid email"
    else:
        flag = -1
        message = "Invalid email."


    if flag ==-1:
        print(message)
        return False, message
    else:
        print(message)
        return True, message

# Uncomplete method
def checkNoHaveSymbol(word: str):
    special_char = False
    print('WORD LANGUAGE = ', detect(word))
    intonation_marks = '่้๊๋'
    alphabets_marks = 'แฺิ์ืใโเ็า..๐ๆไำะํัีูุึ' 
    regexp = re.compile('[^ก-ฮ{intonation_marks}a-zA-Z]+')
    if regexp.search(word):
        special_char = True
        print('special_char')
    else:
        print('no have special char')

def detect_special_characer(pass_string): 
    regex= re.compile('[@_!#$%^&*()<>?/\\\|}{~:[\]]') 
    if(regex.search(pass_string) == None): 
        print('no have special char')
        res = False
    else: 
        print('special_char')
        res = True
    return(res)