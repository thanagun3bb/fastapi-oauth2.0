import jwt
from fastapi import Security, HTTPException
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer
from passlib.context import CryptContext
from datetime import datetime, timedelta

class AuthHandler():
    security = HTTPBearer
    pwd_cxt = CryptContext(schemes=["bcrypt"], deprecated="auto")
    SECRET_KEY = "3BBJWTService"
    ACCESS_TOKEN_EXPIRE_MINUTES = 15
    ALGORITHM = 'HS256'
    def get_password_hash(self, password):
        return self.pwd_cxt.hash(password)

    def verify_password(self, plain_password, hashed_password):
        return self.pwd_cxt.verify(plain_password, hashed_password)

    def encode_token(self, user_id):
        payload = {
            'exp': datetime.utcnow() + timedelta(days=0, minutes=ACCESS_TOKEN_EXPIRE_MINUTES),
            'int': datatime.utcnow(),
            'sub': user_id
        }
        return jwt.encode(
            payload,
            self.SECRET_KEY,
            algorithm=self.ALGORITHM
        )

    def decode_token(self, token):
        try:
            payload = jwt.decode(token, self.secret, algorithms=['HS256'])
            return payload['sub']
        except jwt.ExpiredSignatureError:
            raise HTTPException(status_code=401, detail='Signature has expired')
        except jwt.InvalidTokenError as e:
            raise HTTPException(status_code=401, detail='Invalid token')

    def auth_wrapper(self, auth: HTTPAuthorizationCredentials = Security(security)):
        return self.decode_token(auth.credentials)