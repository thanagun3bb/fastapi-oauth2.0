
from fastapi import APIRouter, Request, Body, Depends, status, HTTPException
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from utils.validate import checkUsername, checkPassword, checkEmail,detect_special_characer
import schemas
from pydantic import ValidationError
from passlib.hash import bcrypt
from redisdb.user import RedisUser
# from db.database import User
router = APIRouter(
    prefix= "/signup",
    tags=["signup"]
)


@router.post("/")
async def signup_user(request:Request, register:schemas.Register = Body(...)):
    try:
        is_valid, message = checkUsername(register.username)
        # Check invalid username
        if not is_valid:
            raise HTTPException(status_code=404, detail=message) 
        # Check invalid password
            is_valid, message = checkPassword(register.password)
        if not is_valid:
            raise HTTPException(status_code=404, detail=message) 
        # Check invalid confirm password
        if not register.password == register.confirm_password:
            raise HTTPException(status_code=404, detail="Confirm password invalid") 
        # Check email invalid
        is_valid, message = checkEmail(register.email)
        if not is_valid:
            raise HTTPException(status_code=404, detail=message)
        # Check name have any symbol
        if detect_special_characer(register.name):
            raise HTTPException(status_code=404, detail="Don't input character symbol in name")
            
        # Have new user in redis
        # redis_user = RedisUser.find(register.email)
        # if redis_user == None:
            print('have new user in redis')
            # Check have new user in mongodb
            # Check have user already exist 
            mongodb_user = await request.app.mongodb["user"].find_one(
                {"email": register.email}
            )
            # Have new user register save redis and mongodb
            if mongodb_user == None:
                print('Have new user register save redis and mongodb')
                user = schemas.User(
                    username=register.username,
                    password_hash=bcrypt.hash(register.password),
                    name = register.name,
                    email = register.email,
                    tel_number = register.tel_number
                )
                # Convert to json and save redis
                user_json=jsonable_encoder(user)
                # Save in Redis
                # RedisUser.save(user.email,user_json)
                # Save in mongodb
                new_user = await request.app.mongodb["user"].insert_one(user_json)
                created_task = await request.app.mongodb["user"].find_one(
                    {"_id": new_user.inserted_id}
                )
                return {"message":"success"}
            else:
                raise HTTPException(status_code=404, detail="Username already exist in database")
        # else:
        #     raise HTTPException(status_code=404, detail="Username already exist in redis")
    except ValidationError as e:
        print("error = ",e.json())