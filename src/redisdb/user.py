import redis
from config import RedisSettings
from schemas import User
import json

class RedisUser():
    connection = None

    def find(key: str):
        connection = redis.Redis(host=RedisSettings.REDIS_HOST, port=RedisSettings.REDIS_PORT)
        raw_user = connection.get(key)
        if raw_user == None:
            return None
        user = json.loads(raw_user)
        print("Username = ", user["username"])
        return user

    def save(key: str, user: dict):
        connection = redis.Redis(host=RedisSettings.REDIS_HOST, port=RedisSettings.REDIS_PORT)
        print("redis save",type(user))
        json_user = json.dumps(user)
        connection.set(key,json_user)
        # result = conn.get(key)