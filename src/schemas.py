from typing import List, Optional
from pydantic import BaseModel, Field

class Register(BaseModel):
    username:str = Field(...)
    password:str = Field(...)
    confirm_password:str = Field(...)
    name: str = Field(...)
    email:str = Field(...)
    tel_number:Optional[str] = Field(...)
    class Config: 
        orm_mode = True
        schema_extra = {
            "example": {
                "username": "JohnDoe",
                "password": "Abcd123",
                "confirm_password": "Abcd123",
                "name": "My important task",
                "email": "john@test.com",
                "tel_number": "xxx-xxx-xxxx",
            }
        }

class User(BaseModel):
    username:str = Field(...)
    password_hash:str = Field(...)
    name: str = Field(...)
    email:str = Field(...)
    tel_number:Optional[str] = Field(...)
    class Config: 
        orm_mode = True
        schema_extra = {
            "example": {
                "username": "JohnDoe",
                "password": "Abcd123",
                "name": "My important task",
                "email": "john@test.com",
                "tel_number": "xxx-xxx-xxxx",
            }
        }
