import uvicorn
from fastapi import FastAPI, Request, status
from fastapi.security import OAuth2PasswordBearer
from fastapi.encoders import jsonable_encoder
from fastapi.responses import JSONResponse
from routers import signup
from motor.motor_asyncio import AsyncIOMotorClient
from config import DatabaseSettings
import schemas

app = FastAPI()

@app.on_event("startup")
async def startup_db_client():
    print("startup_db_client")
    app.mongodb_client = AsyncIOMotorClient(DatabaseSettings.DB_URL)
    app.mongodb = app.mongodb_client[DatabaseSettings.DB_NAME]

@app.on_event("shutdown")
async def shutdown_db_client():
    app.mongodb_client.close()

@app.post('/hello')
async def hello(request:Request, register: schemas.Register):
    task=jsonable_encoder(register)
    new_task = await request.app.mongodb["user"].insert_one(task)
    created_task = await request.app.mongodb["user"].find_one(
            {"_id": new_task.inserted_id}
    )
    return created_task

app.include_router(signup.router)

if __name__ == "__main__":
    uvicorn.run(
        "main:app",
        host="0.0.0.0",
        reload=True,
        port=8000,
    )
